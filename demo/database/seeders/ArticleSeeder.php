<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{

    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Article::create([
                'title' => 'title2' . $i,
                'body' => 'body2' . $i
            ]);
        }
    }
}
