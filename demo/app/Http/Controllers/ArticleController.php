<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function index()
    {
        return response()->json([
            'status code' => 200,
            'message' => 'list article',
            'articles' => Article::all()
        ]);
    }

    public function store(StorePostRequest $request)
    {
        $article = Article::create($request->all());
        return response()->json([
            'status code' => 201,
            'message' => "create article success",
            'article' => $article
        ]);
    }

    public function show($id)
    {
        $article = Article::find($id);
        if ($article) {
            return response()->json(
                [
                    'status code' => 200,
                    'message' => 'show artilce ' . $id . ' success',
                    'article' => $article
                ]
            );
        }
        return response()->json(
            [
                'status code' => 404,
                'message' => 'Not found'
            ]
        );
    }


    public function update(StorePostRequest $request, $id)
    {
        $article = Article::find($id);
        if ($article) {
            $article->update($request->all());
            return response()->json(
                [
                    'status code' => 200,
                    'message' => 'edit article ' . $id . ' success',
                    'article' => $article
                ]
            );
        }
        return response()->json(
            [
                'status code' => 404,
                'message' => 'Not found'
            ]
        );
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        if ($article) {
            $article->delete();
            return response()->json(
                [
                    'status code' => 204,
                    'message' => 'delete article ' . $id . ' success'
                ],
            );
        }
        return response()->json(
            [
                'status code' => 404,
                'message' => 'Not found'
            ]
        );
    }
}
